npm-telegram-bot
==================
Simple inline mode [Telegram](https://telegram.me) bot that queries the [npm register](https://npmjs.com) for packages matching the search terms you type in.

## Heroku deploy

Click the button down below to deploy this to heroku

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/rjmunhoz/npmjs-telegram-bot/blob/master/)
